#include "connection.hpp"
#include "node.hpp"

Connection::Connection(Node* n1, Node* n2) {
    node1 = n1;
    node2 = n2;
    length = node1->distanceTo(*node2);
}
