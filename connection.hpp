#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include "node.hpp"

class Connection {
        public:
                Connection(Node*, Node*);
                double length;
                Node* node1;
                Node* node2;
};
#endif
