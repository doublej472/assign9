#include <iostream>
#include <random>
#include <vector>
#include <SFML/Graphics.hpp>

#include "node.hpp"
#include "connection.hpp"

// Define the window size
constexpr int WINDOW_WIDTH {800};
constexpr int WINDOW_HEIGHT {600};

// How big each node is drawn on screen
constexpr double NODE_RADIUS {1.5};
constexpr int NUM_NODES {1000};

// Update speed
constexpr int FRAMES_PER_UPDATE {1};

using namespace std;

// Random number classes
std::mt19937 rng(std::random_device{}());
std::uniform_int_distribution<int> distributionX(0, WINDOW_WIDTH);
std::uniform_int_distribution<int> distributionY(0, WINDOW_HEIGHT);
    
// Used for Prim's algorithm
std::vector<Node*> nodes;
std::vector<Node*> nodesAdded;

// Only used for drawing
std::vector<Connection> connections;

// returns random numbers as coords
std::vector<double> getRandomCoords() {
	std::vector<double> ret;

	ret.emplace_back(distributionX(rng));
	ret.emplace_back(distributionY(rng));

	return ret;
}

// Given connections, draws lines on screen
void drawLines(sf::RenderWindow* window, std::vector<Connection>* conns) {
    sf::VertexArray lines(sf::Lines, conns->size()*2);
    for (int i = 0; i < conns->size(); i++) {
        lines[i*2] = sf::Vector2f(conns->at(i).node1->x, conns->at(i).node1->y);
        lines[(i*2)+1] = sf::Vector2f(conns->at(i).node2->x, conns->at(i).node2->y);
    }
    window->draw(lines);
}

// Frees memory
void cleanUp() {
	for (Node* n : nodes) {
		delete n;
	}
	for (Node* n : nodesAdded) {
		delete n;
	}

	nodes.clear();
	nodesAdded.clear();
	connections.clear();
}

void reset() {
	// Clear memory if required
	cleanUp();
    
	// Add random nodes
	for (int i = 0; i < NUM_NODES; i++) {
		std::vector<double> coords = getRandomCoords();
		Node* n = new Node(coords[0], coords[1]);
		nodes.emplace_back(n);
	}
    
	// Get first node
    nodesAdded.emplace_back(nodes.back());
    nodes.pop_back();
}


int main(int argc, char* argv[]) {
    // Make sure that a crappy seed doesn't ruin the random numbers
	rng.discard(700000);

    // Create window and set up framerate
	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Fun Node Stuff!");
	window.setFramerateLimit(60);

	// Initialize Nodes
	reset();

	// Used to update the algorithm slower
    int mod = 0;
    
    // Core loop
	while (window.isOpen()) {
        if (nodes.size() > 0 && ++mod % FRAMES_PER_UPDATE == 0) {
            // Get node closest to whats already added
            int si, ei;
            double shortest = 999999;
            for (int i = 0; i < nodesAdded.size(); i++) {
                for (int j = 0; j < nodes.size(); j++) {
                    if (nodesAdded[i]->distanceTo(*nodes[j]) < shortest) {
                        shortest = nodesAdded[i]->distanceTo(*nodes[j]);
                        si = i;
                        ei = j;
                    }
                }
            }

            // Add closest node to added nodes
            nodesAdded.emplace_back(nodes[ei]);
            nodes.erase(nodes.begin() + ei);

            // Add connection between nodes
            Connection c(nodesAdded[si], nodesAdded.back());
            connections.emplace_back(c);
        }

		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::KeyPressed:
					reset();
					break;
				default:
					break;
			}
		}
		// Draw the window
		window.clear(sf::Color::Black);

		sf::CircleShape ns;
		ns.setRadius(NODE_RADIUS);
		ns.setFillColor(sf::Color::White);
		ns.setOrigin(NODE_RADIUS, NODE_RADIUS);
        
        drawLines(&window, &connections);

        for (Node* n : nodes) {
			ns.setPosition(n->x, n->y);
			window.draw(ns);
		}
		
        ns.setFillColor(sf::Color::Green);
        for (Node* n : nodesAdded) {
			ns.setPosition(n->x, n->y);
			window.draw(ns);
		}

		window.display();
	}

	// Cleanup nodes
	for (Node* n : nodes) {
		delete n;
	}
	for (Node* n : nodesAdded) {
		delete n;
	}

	nodes.clear();
	nodesAdded.clear();
	connections.clear();

	return 0;
}
