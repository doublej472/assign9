#include <cmath>
#include "node.hpp"

Node::Node(double xx, double yy) {
        x = xx;
        y = yy;
}

double Node::distanceTo(Node& other) {
        return pow(x - other.x, 2.0) + pow(y - other.y, 2.0);
}
