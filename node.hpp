#ifndef NODE_HPP
#define NODE_HPP
class Node {
        public:
                Node(double, double);
                double x, y;
                double distanceTo(Node&);
};
#endif
